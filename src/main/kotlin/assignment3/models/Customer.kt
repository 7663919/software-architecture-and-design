package assignment3.models

data class Customer(
        var customerId : Int = 0,
        var firstName : String = "",
        var lastName : String = "",
        var address : String = "",
        var phoneNumber : String = ""
)