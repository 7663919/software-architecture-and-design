package assignment3.models

import java.time.LocalDate

data class TaxInvoice (
        override val customer : Customer,
        val salesInvoice: SalesInvoice,
        var deadline : LocalDate = LocalDate.now().plusDays(30),
        val issueDate : LocalDate = LocalDate.now(),
        var paidState : Boolean = false
) : Invoice(customer)