package assignment3.models

import java.time.LocalDate
import java.util.*

/**
 * Data holder class that contains a single piece of information about a recreational vehicle
 * or trailer that HTV has acquired and is selling, or has sold.
 */
data class Vehicle(
        var vinNumber : String = "",
        var model : String = "",
        var year : LocalDate = LocalDate.now(),
        var manufacturer : String = "",
        var baseCost : Double = 0.0
)
{
    override fun toString(): String {
        return manufacturer + " " + model + " " + year
    }
}