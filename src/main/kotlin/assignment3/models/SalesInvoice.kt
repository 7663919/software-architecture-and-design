package assignment3.models

import assignment3.Domain.DealerInstalledOptions
import assignment3.Domain.Salesperson
import java.time.LocalDate
import java.util.*

data class SalesInvoice(
        override val customer : Customer = Customer(),
        val tradeInVehicle : Vehicle = Vehicle(),
        val purchaseVehicle : Vehicle = Vehicle(),
        val dealerInstalledOptions: DealerInstalledOptions = DealerInstalledOptions(),
        val negotiatedPrice : Double = 0.0,
        val applicableTaxes : Double = 0.0,
        val licenseFees : Double = 0.0,
        val issueDate : LocalDate = LocalDate.now(),
        val soldBy : Salesperson
) : Invoice(customer)