package assignment3.app

import assignment3.Seed.DummyDatabase
import assignment3.view.HomeView
import assignment3.view.MainView
import javafx.stage.Stage
import tornadofx.App

class MyApp: App(HomeView::class, Styles::class) {
    override fun start(stage : Stage) {
        super.start(stage)
        stage.width = 800.0
        stage.height = 900.0
        stage.isResizable = false

        // Seed data for application.
        DummyDatabase.seedData()
    }
}