package assignment3.ViewModel

import assignment3.models.Vehicle
import tornadofx.*

class VehicleViewModel (vehicle : Vehicle) : ItemViewModel<Vehicle>(vehicle) {

    var vinNumber = bind(Vehicle::vinNumber)
    var model = bind(Vehicle::model)
    var year = bind(Vehicle::year)
    var manufacturer = bind(Vehicle::manufacturer)
    var baseCost = bind(Vehicle::baseCost)

    /**
     * Updates the information stored in the bound vehicle object based on the data provided
     * by the passed in parameter.
     */
    fun updateVehicleData(updatedVehicle : Vehicle) {
        vinNumber.value = updatedVehicle.vinNumber
        model.value = updatedVehicle.model
        year.value = updatedVehicle.year
        manufacturer.value = updatedVehicle.manufacturer
        baseCost.value = updatedVehicle.baseCost
    }

    /**
     * Reset all the values associated with this view model.
     */
    fun resetPropertyValues() {
        vinNumber.value = ""
        model.value = ""
        manufacturer.value = ""
        baseCost.value = 0.0
    }
}