package assignment3.ViewModel

import assignment3.models.Customer
import tornadofx.*

class CustomerDetailsViewModel (customer : Customer) : ItemViewModel<Customer>(customer)
{
    var firstName = bind(Customer::firstName)
    var lastName = bind(Customer::lastName)
    var address =  bind(Customer::address)
    var phoneNumber = bind(Customer::phoneNumber)

    /**
     * Reset all the values associated with this view model.
     */
    fun resetPropertyValues() {
        firstName.value = ""
        lastName.value = ""
        address.value = ""
        phoneNumber.value = ""
    }
}