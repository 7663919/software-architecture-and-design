package assignment3.ViewModel

import assignment3.Domain.Inventory
import assignment3.Domain.Salesperson
import assignment3.models.Invoice
import assignment3.models.SalesInvoice
import assignment3.models.Vehicle
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import tornadofx.*

class SalesInvoiceViewModel (private val salesInvoice : SalesInvoice) : ItemViewModel<SalesInvoice>(salesInvoice)
{
    private val inventory = Inventory()

    val customerDetailsViewModel = CustomerDetailsViewModel(salesInvoice.customer)
    val tradeInVehicleViewModel = VehicleViewModel(salesInvoice.tradeInVehicle)
    val purchaseVehicleViewModel = VehicleViewModel(salesInvoice.purchaseVehicle)
    val dealerInstalledOptionsViewModel = DealerInstalledOptionsViewModel(salesInvoice.dealerInstalledOptions)

    var negotiatedPrice = bind(SalesInvoice::negotiatedPrice)
    var applicableTaxes = bind(SalesInvoice::applicableTaxes)
    var licenseFees = bind(SalesInvoice::licenseFees)
    var issueDate = bind(SalesInvoice::issueDate)
    var soldBy = bind(SalesInvoice::soldBy)

    /**
     * Validate all view models associated to this view model, including this one.
     * This ensures that any views that are displaying errors can run this to get the context back.
     */
    fun validateAll() {
        validate()

        customerDetailsViewModel.validate()
        tradeInVehicleViewModel.validate()
        purchaseVehicleViewModel.validate()
        dealerInstalledOptionsViewModel.validate()
    }

    /**
     * Checks if all the view models associated to this view model, and including this view model are valid.
     */
    fun checkAllIsValid() : Boolean{

        return isValid && customerDetailsViewModel.isValid && tradeInVehicleViewModel.isValid && purchaseVehicleViewModel.isValid
                && dealerInstalledOptionsViewModel.isValid
    }

    /**
     * Checks the required properties of the trade in vehicle to determine if one is being added.
     * If any of the required properties are detected then it should be expected that the other
     * required properties will also be filled.
     */
    fun isAddingTradeInVehicle() : Boolean {
        return tradeInVehicleViewModel.vinNumber.isDirty || tradeInVehicleViewModel.model.isDirty
                || tradeInVehicleViewModel.manufacturer.isDirty
    }

    /**
     * Gets the vehicles for sale through the inventory.
     */
    fun getVehiclesForSale() : List<Vehicle> {
        return inventory.getVehiclesForSale()
    }

    /**
     * Asks the sales person to produce an invoice from the data gathered here.
     */
    fun produceInvoice(salesPerson : Salesperson) {
        salesPerson.produceInvoice(salesInvoice)
    }

    /**
     * Reset the values associated with this viewmodel.
     */
    fun resetPropertyValues() {
        negotiatedPrice.value = 0.0
        applicableTaxes.value = 0.0
        licenseFees.value = 0.0
        customerDetailsViewModel.resetPropertyValues()
        tradeInVehicleViewModel.resetPropertyValues()
        purchaseVehicleViewModel.resetPropertyValues()
        dealerInstalledOptionsViewModel.resetPropertyValues()
    }
}