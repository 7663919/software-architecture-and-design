package assignment3.ViewModel

import assignment3.Domain.DealerInstalledOptions
import tornadofx.*

class DealerInstalledOptionsViewModel (dealerInstalledOptions: DealerInstalledOptions) : ItemViewModel<DealerInstalledOptions>(dealerInstalledOptions)
{
    var leatherSeats = bind(DealerInstalledOptions::leatherSeats)
    var roofRack = bind(DealerInstalledOptions::roofRack)
    var racingStripes = bind(DealerInstalledOptions::racingStripes)
    var bullBar = bind(DealerInstalledOptions::bullBar)
    var sideSteps = bind(DealerInstalledOptions::sideSteps)
    var extendedWarranty = bind(DealerInstalledOptions::extendedWarranty)

    /**
     * Reset all the values associated with this view model.
     */
    fun resetPropertyValues() {
        leatherSeats.value = false
        roofRack.value = false
        racingStripes.value = false
        bullBar.value = false
        sideSteps.value = false
        extendedWarranty.value = false
    }
}