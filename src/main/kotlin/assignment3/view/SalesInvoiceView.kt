package assignment3.view

import assignment3.Domain.Inventory
import assignment3.Domain.Salesperson
import assignment3.Seed.DummyDatabase
import assignment3.ViewModel.CustomerDetailsViewModel
import assignment3.ViewModel.SalesInvoiceViewModel
import assignment3.ViewModel.VehicleViewModel
import assignment3.models.Customer
import assignment3.models.SalesInvoice
import assignment3.models.Vehicle
import javafx.collections.FXCollections
import javafx.scene.control.Alert
import tornadofx.*

class SalesInvoiceView : View("Generate Sales Invoice")
{

    private val salesPerson = DummyDatabase.salesPeople.first()
    private val salesInvoice = SalesInvoice(soldBy = salesPerson)
    private val salesInvoiceViewModel = SalesInvoiceViewModel(salesInvoice)

    override val root = form {

        fieldset("Select Vehicle") {
            field ("Available vehicles"){
                combobox<Vehicle> {
                    items =  FXCollections.observableArrayList(salesInvoiceViewModel.getVehiclesForSale())

                    selectionModel.selectedItemProperty().onChange {

                        if(it != null)
                        {
                            salesInvoiceViewModel.purchaseVehicleViewModel.updateVehicleData(it)
                        }
                    }
                }
            }

            hbox (20){
                vbox {
                    field("Vin number") {
                        textfield {
                            bind(salesInvoiceViewModel.purchaseVehicleViewModel.vinNumber)
                        }
                    }.isDisable = true

                    field("Badge") {
                        textfield {
                            bind(salesInvoiceViewModel.purchaseVehicleViewModel.manufacturer)
                        }
                    }.isDisable = true

                    field("Manufactured Date") {
                        datepicker (salesInvoiceViewModel.purchaseVehicleViewModel.year)
                    }.isDisable = true
                }

                vbox {
                    field("Base cost") {
                        textfield {
                            bind(salesInvoiceViewModel.purchaseVehicleViewModel.baseCost)
                            filterInput { it.controlNewText.isDouble() }
                            isEditable = false
                        }
                    }.isDisable = true

                    field("Model") {
                        textfield {
                            bind(salesInvoiceViewModel.purchaseVehicleViewModel.model)
                        }
                    }.isDisable = true
                }
            }
        }

        fieldset("Dealer installed options") {
            hbox (20){
                vbox {
                    field {
                        checkbox("Leather seats").bind(salesInvoiceViewModel.dealerInstalledOptionsViewModel.leatherSeats)
                    }

                    field {
                        checkbox("Roof rack").bind(salesInvoiceViewModel.dealerInstalledOptionsViewModel.roofRack)
                    }
                }

                vbox {
                    field {
                        checkbox("Racing stripes").bind(salesInvoiceViewModel.dealerInstalledOptionsViewModel.racingStripes)
                    }

                    field {
                        checkbox("Bull bar").bind(salesInvoiceViewModel.dealerInstalledOptionsViewModel.bullBar)
                    }
                }

                vbox{
                    field {
                        checkbox("Extended warrant").bind(salesInvoiceViewModel.dealerInstalledOptionsViewModel.extendedWarranty)
                    }
                }
            }
        }

        fieldset("Applicable Prices") {
            hbox(20) {
                vbox {
                    field ("Negotiated Price"){
                        textfield {
                            bind(salesInvoiceViewModel.negotiatedPrice)
                            filterInput { it.controlNewText.isDouble() }

                            validator {
                                if(it != null && it.isDouble())
                                {
                                    if(it.toDouble() > 0.0)
                                    {
                                        null
                                    }
                                    else
                                    {
                                        error("Purchase cost can't be zero")
                                    }
                                }
                                else
                                {
                                    error("Purchase cost must be a valid number greater than zero")
                                }
                            }
                        }.required()
                    }

                    field ("License Fees"){
                        textfield {
                            bind(salesInvoiceViewModel.licenseFees)
                            filterInput { it.controlNewText.isDouble() }
                        }.required()
                    }
                }

                vbox{
                    field ("Applicable Taxes"){
                        textfield {
                            bind(salesInvoiceViewModel.applicableTaxes)
                            filterInput { it.controlNewText.isDouble() }

                            validator {
                                if(it != null && it.isDouble())
                                {
                                    if(it.toDouble() > 0.0)
                                    {
                                        null
                                    }
                                    else
                                    {
                                        error("Applicable taxes can't be zero")
                                    }
                                }
                                else
                                {
                                    error("Applicable taxes must be a valid cost greater than zero")
                                }
                            }
                        }.required()
                    }
                }
            }

        }

        fieldset("Trade in Vehicle") {
            hbox(20) {
                vbox {
                    field("VIN Number") {
                        textfield(salesInvoiceViewModel.tradeInVehicleViewModel.vinNumber) {
                            validator {
                                // Validate only if user is adding a trade in vehicle.
                                if(salesInvoiceViewModel.isAddingTradeInVehicle() && it.toString().isEmpty())
                                {
                                    error("Must be filled when adding trade in vehicle!")
                                }
                                else
                                {
                            null
                                }
                            }
                        }
                    }

                    field("Model") {
                        textfield(salesInvoiceViewModel.tradeInVehicleViewModel.model) {
                            validator {
                                // Validate only if user is adding a trade in vehicle.
                                if(salesInvoiceViewModel.isAddingTradeInVehicle() && it.toString().isEmpty())
                                {
                                    error("Must be filled when adding trade in vehicle!")
                                }
                                else
                                {
                                    null
                                }
                            }
                        }
                    }

                    field("Base Cost") {
                        textfield {
                            filterInput { it.controlNewText.isDouble() }
                            bind(salesInvoiceViewModel.tradeInVehicleViewModel.baseCost)
                        }
                    }
                }

                vbox {
                    field("Manufactured Date") {
                        datepicker (salesInvoiceViewModel.tradeInVehicleViewModel.year)
                    }

                    field("Badge") {
                        textfield (salesInvoiceViewModel.tradeInVehicleViewModel.manufacturer) {
                            validator {
                                // Validate only if user is adding a trade in vehicle.
                                if (salesInvoiceViewModel.isAddingTradeInVehicle() && it.toString().isEmpty()) {
                                    error("Must be filled when adding trade in vehicle!")
                                } else {
                                    null
                                }
                            }
                        }
                    }
                }
            }
        }

        fieldset ("Customer information") {
            hbox(20) {
                vbox {
                    field("First name") {
                        textfield (salesInvoiceViewModel.customerDetailsViewModel.firstName).required()
                    }

                    field("Phone number") {
                        textfield (salesInvoiceViewModel.customerDetailsViewModel.phoneNumber).required()
                    }
                }

                vbox {
                    field("Last name") {
                        textfield(salesInvoiceViewModel.customerDetailsViewModel.lastName).required()
                    }
                }
            }

            field("Address") {
                textfield(salesInvoiceViewModel.customerDetailsViewModel.address).required()
            }
        }

        buttonbar{
            button("Generate Invoice") {
                setOnAction {
                    salesInvoiceViewModel.validateAll()

                    if(salesInvoiceViewModel.checkAllIsValid()) {
                        salesInvoiceViewModel.produceInvoice(salesPerson)
                        alert(Alert.AlertType.INFORMATION, "Vehicle Sold", "Invoice sent for vehicle")
                        salesInvoiceViewModel.resetPropertyValues()
                        replaceWith<HomeView>()
                    }
                }
            }

            button("Cancel") {
                setOnAction {
                    salesInvoiceViewModel.resetPropertyValues()
                    replaceWith<HomeView>()
                }
            }
        }
    }
}