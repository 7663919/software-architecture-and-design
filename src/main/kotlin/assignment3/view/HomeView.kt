package assignment3.view

import javafx.geometry.Pos
import tornadofx.*

class HomeView : View("Home View")
{
    override val root = vbox(alignment = Pos.CENTER) {

        button("Generate Sales Invoice").setOnAction{
            replaceWith<SalesInvoiceView>()
        }

        button("Quit").setOnAction {
            close()
        }
    }
}