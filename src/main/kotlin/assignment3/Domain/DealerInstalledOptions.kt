package assignment3.Domain

/**
 * Captures a list of dealer installed options that can be added to and updated but not
 * deleted. This class ensures that if the dealer installed options change then any existing
 * invoices prior to the changes will still list the correct items.
 */
data class DealerInstalledOptions(
 var leatherSeats : Boolean = false,
 var roofRack : Boolean = false,
 var racingStripes : Boolean = false,
 var bullBar : Boolean = false,
 var sideSteps : Boolean = false,
 var extendedWarranty : Boolean = false
)