package assignment3.Domain

import assignment3.Seed.DummyDatabase
import assignment3.models.Invoice
import assignment3.models.SalesInvoice
import assignment3.models.Vehicle
import java.time.LocalDate

/**
 * The inventory contains a list of vehicles that HTV have acquired and are selling, or have sold.
 * Invoices are used to indicate if a vehicle has been sold. The same vehicle could have been sold
 * by HTV multiple times to different customers who buy and then trade in their vehicle.
 */
class Inventory
{
    /**
     * Returns all the vehicles that HTV currently owns or have sold.
     * Other classes can filter the vehicles down from this list based on the criteria that they are after.
     */
    fun getVehicles() : List<Vehicle> {
        return DummyDatabase.vehicles
    }

    /**
     * Get a single vehicle that matches the vin number provided.
     * Will return null if vin number not found.
     */
    fun getVehicle(vinNumber : String) : Vehicle? {
        return getVehicles().firstOrNull { v -> v.vinNumber == vinNumber }
    }

    /**
     * Returns all the invoices (tax or sales) that have ever been produced. The calling class can filter the information
     * down based on their needs.
     */
    fun getInvoices() : List<Invoice> {
        return DummyDatabase.invoices
    }

    /**
     * Get the full list of vehicles and filter them down by those that have a sales invoice attached to them.
     * The remaining vehicles are for sale.
     */
    fun getVehiclesForSale() : ArrayList<Vehicle>
    {
        val returnVehicles = ArrayList<Vehicle>()

        val vehicles = DummyDatabase.vehicles
        val salesInvoices = DummyDatabase.invoices.filter { invoice -> invoice is SalesInvoice}

        var addVehilceToList : Boolean

        for(vehicle in vehicles)
        {
            addVehilceToList = true

            for(salesInvoice in salesInvoices)
            {
                if(salesInvoice is SalesInvoice) {
                    if (vehicle == salesInvoice.purchaseVehicle) {
                        addVehilceToList = false
                        break
                    }
                }
            }

            // Only add vehicle to return list if sales invoice not found.
            if(addVehilceToList)
            {
                returnVehicles += vehicle
            }
        }

        return returnVehicles
    }

    /**
     * Add acquired vehicle to list of vehicles.
     */
    fun addVehicle(vehicle : Vehicle)
    {
        DummyDatabase.vehicles += vehicle
    }

    /**
     * Stores the provided invoice.
     * It doesn't matter if its a tax invoice or sales invoice. It deals with it.
     */
    fun addInvoice(invoice: Invoice)
    {
        DummyDatabase.invoices += invoice
    }
}