package assignment3.Domain

import assignment3.models.Invoice

/**
 * An interface that enforces that employees that are sub classes will be able to
 * produce some form of invoice.
 */
interface Employee {
    fun produceInvoice(invoice : Invoice)
}