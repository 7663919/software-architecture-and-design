package assignment3.Domain

import assignment3.Seed.DummyDatabase
import assignment3.models.Customer

/**
 * Stores information about all customers that have purchased a vehicle.
 * This class will allow you to modify a single customer record.
 */
class CustomerList {

    /**
     * Get all customers that are stored.
     * Note: this function can be used to update a particular customer.
     */
    fun getCustomers(): List<Customer> {
        return DummyDatabase.customers
    }

    /**
     * Get a list of customers that have the same mobile number.
     */
    fun findCustomers(phoneNumber : String) : List<Customer> {

        val foundCustomers = ArrayList<Customer>()

        for(customer in DummyDatabase.customers)
        {
            if(customer.phoneNumber == phoneNumber)
            {
                foundCustomers += customer
            }
        }

        return foundCustomers
    }

    /**
     * Adds the provided customer to storage.
     */
    fun createCustomer(customer : Customer) {
        customer.customerId = getCustomers().size + 1
        DummyDatabase.customers += customer
    }
}