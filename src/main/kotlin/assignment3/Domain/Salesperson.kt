package assignment3.Domain

import assignment3.models.Customer
import assignment3.models.Invoice
import assignment3.models.SalesInvoice
import assignment3.models.Vehicle

/**
 * A salesperson is an employee that can produce sales invoices after the successful purchase
 * of a vehicle. They are responsible for creating customers and can list the vehicles they have
 * sold (if any).
 */
class Salesperson : Employee {

    // Customer / Inventory list can be set if an established one is already under use.
    var customerList = CustomerList()
    var inventory : Inventory = Inventory()

    /**
     * Extracts information from the provided invoice (if it is a sales invoice) and generates the relevant customer,
     * and trade in vehicle details before storing the data.
     */
    override fun produceInvoice(invoice: Invoice) {

        if(invoice is SalesInvoice)
        {
            // Add customer.
            customerList.createCustomer(invoice.customer)

            // Add trade in vehicle if provided.
            invoice.tradeInVehicle.let {
                if(it.vinNumber.isNotEmpty()) {
                    inventory.addVehicle(it)
                }
            }

            // Add sales invoice.
            inventory.addInvoice(invoice)
        }
    }

    /**
     * Gets the invoices that this sales person has produced.
     */
    fun getSalesInvoicesProduced() : List<Invoice> {
        return inventory.getInvoices().filter{ invoice -> invoice is SalesInvoice && invoice.soldBy == this}
    }

    /**
     * List vehicles that this sales person has sold.
     */
    fun listVehiclesSold(): List<Vehicle> {

        val vehiclesSold = ArrayList<Vehicle>()
        val invoices = getSalesInvoicesProduced()
        for(invoice in invoices)
        {
            if(invoice is SalesInvoice)
            {
                vehiclesSold += invoice.purchaseVehicle
            }
        }

        return vehiclesSold
    }

    /**
     * Checks if the given vehicle has been sold or is available for sale.
     * returns true if the vehicle is available for sale.
     */
    fun isVehicleForSale(vehicle : Vehicle) : Boolean{
        return inventory.getVehiclesForSale().contains(vehicle)
    }

    /**
     * Determines if the provided vehicle has ever been purchased by the provided customer.
     */
    fun hasVehicleBeenSoldToCustomer(vehicle: Vehicle, customer : Customer) : Boolean {

        // Filter the invoices down to those that the customer has.
        val filteredInventory = inventory.getInvoices().filter {
            invoice -> invoice is SalesInvoice
                && invoice.customer == customer
                && invoice.purchaseVehicle == vehicle
        }

        return filteredInventory.isNotEmpty()
    }
}