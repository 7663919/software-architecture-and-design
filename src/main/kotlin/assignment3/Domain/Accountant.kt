package assignment3.Domain

import assignment3.models.Invoice
import assignment3.models.SalesInvoice
import assignment3.models.TaxInvoice
import java.util.*

/**
 * An accountant is an employee that can produce tax invoices after a sales invoice has been
 * generated. They can track Tax invoices and update them when they have been paid.
 */
class Accountant : Employee
{
    var inventory : Inventory = Inventory()

    /**
     * Given the information stored within a sales invoice this method will take the necessary steps to produce a tax
     * invoice and store it.
     */
    override fun produceInvoice(invoice: Invoice) {
        if(invoice is SalesInvoice) {
            inventory.addInvoice(TaxInvoice(customer = invoice.customer, salesInvoice = invoice))
        }
    }

    /**
     * Returns a list of all tax invoices that have been produced. This list will contain tax invoices which have
     * expired, have been paid in full, or have yet to be paid.
     */
    fun getTaxInvoices() : List<Invoice> {
        return inventory.getInvoices().filter { invoices -> invoices is TaxInvoice}
    }

    /**
     * Get the tax invoice that has the provided sales invoice or null if the sales invoice can't be found.
     */
    fun getTaxInvoiceForSalesInvoice(salesInvoice : SalesInvoice) : TaxInvoice? {

        val taxInvoices = getTaxInvoices()

        if(taxInvoices.isNotEmpty()) {
            return getTaxInvoices().firstOrNull { invoice -> (invoice as TaxInvoice).salesInvoice == salesInvoice } as TaxInvoice
        }

        return null
    }

    /**
     * Get a list of unpaid tax invoices.
     */
    fun getUnpaidTaxInvoices() : List<Invoice> {
        return inventory.getInvoices().filter { invoice -> invoice is TaxInvoice && !invoice.paidState }
    }
}