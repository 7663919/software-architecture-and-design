package assignment3.Seed

import assignment3.Domain.Salesperson
import assignment3.models.Customer
import assignment3.models.Invoice
import assignment3.models.SalesInvoice
import assignment3.models.Vehicle
import java.time.LocalDate
import javax.xml.bind.ValidationEventHandler

/**
 * Mocked up data for demonstration purposes that simulates a database only in
 * terms of retrieving data.
 */
class DummyDatabase {
    companion object {

        // Vehicles.
        private val BMW_M3 = Vehicle(vinNumber = "1HGBH41JXMN109186", baseCost = 18000.00, manufacturer = "BMW", model = "M3", year = LocalDate.ofYearDay(2009, 1))
        private val TOYOTA_HILUX = Vehicle(vinNumber = "1324987123KADFKED", baseCost = 30000.00, manufacturer = "Toyota", model = "Hilux", year = LocalDate.ofYearDay(2019, 1))
        private val FORD_RANGER = Vehicle(vinNumber = "OUOQCKJ1349KJI922", baseCost = 56000.00, manufacturer = "Ford", model = "Ranger", year = LocalDate.ofYearDay(2010, 1))
        private val MAZDA_3 = Vehicle(vinNumber = "ALDKFJWE3234CSE43", baseCost = 27000.00, manufacturer = "Mazda", model = "3", year = LocalDate.ofYearDay(2017, 1))
        private val TOYOTA_COROLLA = Vehicle(vinNumber = "1CNZ2MZNCWDKSF234", baseCost = 60000.00, manufacturer = "Toyota", model = "Corolla", year = LocalDate.ofYearDay(2005, 1))
        private val TOYOTA_LANDCRUISER = Vehicle(vinNumber = "13241241324ADSF32", baseCost = 30200.00, manufacturer = "Toyota", model = "LandCruiser", year = LocalDate.ofYearDay(2020, 1))
        private val HYUNDAI_I30 = Vehicle(vinNumber = "99849199AD9849S9D", baseCost = 58000.00, manufacturer = "Hyundai", model = "i30", year = LocalDate.ofYearDay(2015, 1))
        private val MAZDA_CX5 = Vehicle(vinNumber = "995132DS21D6WE2D2", baseCost = 89000.00, manufacturer = "Mazda", model = "CX-5", year = LocalDate.ofYearDay(2013, 1))
        private val HYUNDAI_TUCSON = Vehicle(vinNumber = "KLKCAD2343CAKLJ32", baseCost = 70200.00, manufacturer = "Hyundai", model = "Tucson", year = LocalDate.ofYearDay(2018, 1))
        private val KIA_CERATO = Vehicle(vinNumber = "AADSFQOIJ23425234", baseCost = 1000.00, manufacturer = "Kia", model = "Cerato", year = LocalDate.ofYearDay(2003, 1))
        private val TOYOTA_PRADO = Vehicle(vinNumber = "1655651VASDF1WE51", baseCost = 3000.00, manufacturer = "Toyota", model = "Prado", year = LocalDate.ofYearDay(2000, 1))

        // Customers.
        private val steve = Customer(customerId = 1, firstName = "Steve", lastName = "Rogers", address = "127.0.0.1", phoneNumber = "0412345668")
        private val bruce = Customer(customerId = 2, firstName = "Bruce", lastName = "Wayne", address = "123 Gotham Rd, Gotham", phoneNumber = "0412345668")

        // Sales People.
        private val dwight = Salesperson()

        // SalesInvoices.
        private val steveInvoice = SalesInvoice(customer = steve, tradeInVehicle = MAZDA_3, purchaseVehicle = MAZDA_CX5, negotiatedPrice = 50000.0, applicableTaxes = 50.00, licenseFees = 100.0, soldBy = dwight)
        private val bruceInvoice = SalesInvoice(customer = bruce, tradeInVehicle = TOYOTA_COROLLA, purchaseVehicle = MAZDA_3, negotiatedPrice = 2000.0, applicableTaxes = 2.00, licenseFees = 100.0, soldBy = dwight)


        val invoices = ArrayList<Invoice>()
        val customers = ArrayList<Customer>()
        val vehicles = ArrayList<Vehicle>()
        val salesPeople = ArrayList<Salesperson>()

        /**
         * This function should be called first when program starts before retrieving data.
         */
        fun seedData() {

            // Clear lists.
            invoices.clear()
            customers.clear()
            vehicles.clear()
            salesPeople.clear()

            // Vehicles.
            vehicles += BMW_M3
            vehicles += TOYOTA_HILUX
            vehicles += FORD_RANGER
            vehicles += MAZDA_3
            vehicles += TOYOTA_COROLLA
            vehicles += TOYOTA_LANDCRUISER
            vehicles += HYUNDAI_I30
            vehicles += MAZDA_CX5
            vehicles += HYUNDAI_TUCSON
            vehicles += KIA_CERATO
            vehicles += TOYOTA_PRADO

            // Customers.
            customers += steve
            customers += bruce

            // sales people.
            salesPeople += dwight

            // salesInvoices.
            invoices += steveInvoice
            invoices += bruceInvoice
        }
    }

}