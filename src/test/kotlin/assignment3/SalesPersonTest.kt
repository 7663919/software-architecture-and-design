package assignment3

import assignment3.Domain.DealerInstalledOptions
import assignment3.Domain.Salesperson
import assignment3.Seed.DummyDatabase
import assignment3.models.Customer
import assignment3.models.SalesInvoice
import assignment3.models.Vehicle
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class SalesPersonTest {

    @BeforeEach
    fun init() {
        DummyDatabase.seedData()
    }

    /**
     * Test that the sales person can access a list of vehicles that are currently for sale.
     */
    @Test
    fun testGetVehiclesForSale() {

        val salesPerson = Salesperson()
        val vehicle = Vehicle(vinNumber = "OUOQCKJjtbsfJI922", baseCost = 56000.00, manufacturer = "Nissan", model = "Skyline")

        assertEquals(9, salesPerson.inventory.getVehiclesForSale().size)
        salesPerson.inventory.addVehicle(vehicle)
        assertEquals(10, salesPerson.inventory.getVehiclesForSale().size)
    }

    /**
     * Test that a salesperson is able to produce a sales invoice for a vehicle.
     */
    @Test
    fun testGenerateSalesInvoice() {
        val salesPerson = Salesperson()
        val customer = Customer( firstName = "Bruce", lastName = "Banner", address = "213 Test Street, Test State, Test", phoneNumber = "124234")
        val vehicle = salesPerson.inventory.getVehiclesForSale().first()
        val dealerInstalledOptions = DealerInstalledOptions(leatherSeats = true, racingStripes = true)
        val salesInvoice = SalesInvoice(customer = customer, purchaseVehicle = vehicle, dealerInstalledOptions = dealerInstalledOptions, negotiatedPrice = 30000.0, soldBy = salesPerson)

        assertEquals(0, salesPerson.listVehiclesSold().size)
        salesPerson.produceInvoice(salesInvoice)
        assertEquals(1, salesPerson.listVehiclesSold().size)
        assertEquals(salesInvoice, salesPerson.getSalesInvoicesProduced().first())
    }

    /**
     * Test that we can get the vehicles sold by a salesperson.
     */
    @Test
    fun testGetVehiclesSalespersonHasSold() {
       val salesPerson = DummyDatabase.salesPeople.first()

        assertEquals(2, salesPerson.listVehiclesSold().size)
        assertTrue(salesPerson.inventory.getVehicles().containsAll(salesPerson.listVehiclesSold()))
        assertFalse(salesPerson.inventory.getVehiclesForSale().contains(salesPerson.listVehiclesSold()[0]))
        assertFalse(salesPerson.inventory.getVehiclesForSale().contains(salesPerson.listVehiclesSold()[1]))
    }

    /**
     * Test that when a sales invoice is produced a customer id is set for the customer, and that the id is
     * unique.
     */
    @Test
    fun testGenerateCustomerRecord() {
        val salesPerson = Salesperson()

        val customer = Customer( firstName = "Bruce", lastName = "Banner", address = "213 Test Street, Test State, Test", phoneNumber = "124234")
        val vehicle = salesPerson.inventory.getVehiclesForSale().first()

        assertEquals(0, customer.customerId)

        val salesInvoice = SalesInvoice(customer = customer, purchaseVehicle = vehicle, negotiatedPrice = 30000.0, soldBy = salesPerson)
        assertEquals(0, customer.customerId)

        salesPerson.produceInvoice(salesInvoice)
        assertEquals(3, customer.customerId)

        // Test that the customer ids are unique
        for(storedCustomer in DummyDatabase.customers) {
            for(otherCustomer in DummyDatabase.customers) {
                if(storedCustomer.firstName != otherCustomer.firstName) {
                    assertFalse(storedCustomer.customerId == otherCustomer.customerId)
                }
            }
        }
    }

    /**
     * Tests that a salesperson can determine when a vehicle has been sold.
     */
    @Test
    fun testVehicleSold() {
        val salesPerson = DummyDatabase.salesPeople.first()
        val vehicle = Vehicle(vinNumber = "928734alsdkfj2134", manufacturer = "Nissan", model = "Patrol", baseCost = 101000.50)
        val salesInvoice = SalesInvoice(purchaseVehicle = vehicle, soldBy = salesPerson)

        salesPerson.inventory.addVehicle(vehicle)
        assertTrue(salesPerson.isVehicleForSale(vehicle))

        salesPerson.produceInvoice(salesInvoice)
        assertFalse(salesPerson.isVehicleForSale(vehicle))
    }

    /**
     * Tests whether a vehicle belongs to a customer.
     */
    @Test
    fun testVehicleBelongsToCustomer() {
        val salesPerson = DummyDatabase.salesPeople.first()
        val vehicle = Vehicle(vinNumber = "928734alsdkfj2134", manufacturer = "Nissan", model = "Patrol", baseCost = 101000.50)
        val customer = Customer()
        val otherCustomer = DummyDatabase.customers.first()
        val salesInvoice = SalesInvoice(customer = customer, purchaseVehicle = vehicle, soldBy = salesPerson)

        salesPerson.inventory.addVehicle(vehicle)
        assertFalse(salesPerson.hasVehicleBeenSoldToCustomer(vehicle, customer))
        assertFalse(salesPerson.hasVehicleBeenSoldToCustomer(vehicle, otherCustomer))

        salesPerson.produceInvoice(salesInvoice)
        assertTrue(salesPerson.hasVehicleBeenSoldToCustomer(vehicle, customer))
        assertFalse(salesPerson.hasVehicleBeenSoldToCustomer(vehicle, otherCustomer))
    }

    /**
     * Tests whether the salesperson adds the trade in vehicle as a new vehicle that can be purchased when they create
     * the sales invoice.
     */
    @Test
    fun testTradeInVehicleAddedAsNewVehicle() {
        val salesPerson = Salesperson()
        val tradeInVehicle = Vehicle(vinNumber = "928734alsdkfj2134", manufacturer = "Nissan", model = "Patrol", baseCost = 101000.50)

        val salesInvoice = SalesInvoice(purchaseVehicle = salesPerson.inventory.getVehiclesForSale().first(), tradeInVehicle = tradeInVehicle, soldBy = salesPerson)

        assertFalse(salesPerson.isVehicleForSale(tradeInVehicle))
        salesPerson.produceInvoice(salesInvoice)
        assertTrue(salesPerson.isVehicleForSale(tradeInVehicle))
    }
}