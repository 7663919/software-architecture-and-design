package assignment3

import assignment3.Domain.Accountant
import assignment3.Seed.DummyDatabase
import assignment3.models.SalesInvoice
import assignment3.models.TaxInvoice
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class AccountantTest {

    @BeforeEach
    fun init() {
        DummyDatabase.seedData()
    }

    /**
     * Test that the accountant is able to produce a tax invoice from a sales invoice.
     */
    @Test
    fun testProduceTaxInvoiceFromSalesInvoice() {
        val accountant = Accountant()
        val salesInvoice = DummyDatabase.invoices.first{ invoice -> invoice is SalesInvoice}

        assertEquals(0, accountant.getTaxInvoices().size)

        accountant.produceInvoice(salesInvoice)
        assertEquals(1, accountant.getTaxInvoices().size)

        val storedTaxInvoice = accountant.getTaxInvoices().first() as TaxInvoice
        assertEquals(salesInvoice, storedTaxInvoice.salesInvoice)
    }

    /**
     * Test that the accountant can find and then update a tax invoice given a particular sales invoice.
     */
    @Test
    fun testUpdateTaxInvoice() {
        val accountant = Accountant()
        val salesInvoice = DummyDatabase.invoices.first{ invoice -> invoice is SalesInvoice}

        assertEquals(null, accountant.getTaxInvoiceForSalesInvoice(salesInvoice as SalesInvoice))

        accountant.produceInvoice(salesInvoice)
        assertEquals(1, accountant.getTaxInvoices().size)
        assertEquals(salesInvoice, accountant.getTaxInvoiceForSalesInvoice(salesInvoice)?.salesInvoice)
        assertEquals(false, accountant.getTaxInvoiceForSalesInvoice(salesInvoice)?.paidState)

        accountant.getTaxInvoiceForSalesInvoice(salesInvoice)?.paidState = true
        assertEquals(true, accountant.getTaxInvoiceForSalesInvoice(salesInvoice)?.paidState)
    }

    /**
     * Ensure that an accountant is able to get a list of unpaid tax invoices.
     */
    @Test
    fun testGetUnpaidTaxInvoices() {
        val accountant = Accountant()
        val salesInvoice = DummyDatabase.invoices.first{ invoice -> invoice is SalesInvoice}

        assertTrue(accountant.getUnpaidTaxInvoices().isEmpty())

        accountant.produceInvoice(salesInvoice)
        assertEquals(1, accountant.getUnpaidTaxInvoices().size)
        assertEquals(salesInvoice, accountant.getTaxInvoiceForSalesInvoice(salesInvoice as SalesInvoice)?.salesInvoice)
        assertEquals(false, accountant.getTaxInvoiceForSalesInvoice(salesInvoice)?.paidState)

        accountant.getTaxInvoiceForSalesInvoice(salesInvoice)?.paidState = true
        assertTrue(accountant.getUnpaidTaxInvoices().isEmpty())
    }
}