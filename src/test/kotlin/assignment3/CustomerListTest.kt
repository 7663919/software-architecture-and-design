package assignment3

import assignment3.Domain.CustomerList
import assignment3.Seed.DummyDatabase
import assignment3.models.Customer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class CustomerListTest {

    @BeforeEach
    fun init() {
        DummyDatabase.seedData()
    }

    /**
     * Test that we receive the correct number of customers after seeding the database.
     */
    @Test
    fun testGetCustomers() {
        val customerList = CustomerList()
        assertEquals(2, customerList.getCustomers().size)
    }


    /**
     * Test that we can find a list of customers given our search criteria.
     */
    @Test
    fun testFindListOfCustomers() {
        val customerList = CustomerList()
        val customers = customerList.findCustomers("0412345668")

        assertEquals(2, customers.size)
        assertEquals("Steve", customers[0].firstName)
        assertEquals("Bruce", customers[1].firstName)
    }

    /**
     * Test that we can create a new customer.
     * Creating a new customer should provide us with an incremental id.
     */
    @Test
    fun testCreateNewCustomer() {
        val customerList = CustomerList()
        val customer = Customer(firstName = "Tony", lastName = "Stark", address = "123 Secret Street", phoneNumber = "0234134513")

        customerList.createCustomer(customer)
        assertEquals(3, customerList.getCustomers().size)
        assertEquals(1, customerList.findCustomers("0234134513").size)
        assertEquals(customer.address, customerList.getCustomers()[2].address)
        assertEquals(3, customerList.getCustomers()[2].customerId)
    }

    /**
     * Test that we can update a customer's details.
     */
    @Test
    fun testUpdateCustomerDetails() {
        val customerList = CustomerList()

        customerList.createCustomer(Customer(firstName = "John", lastName = "Cena", address = "6434 Test Place", phoneNumber = "01340343"))
        val customers = customerList.findCustomers("01340343")

        assertEquals(1, customers.size)

        assertEquals("John", customerList.findCustomers("01340343")[0].firstName)
        customers[0].firstName = "Damien"
        assertEquals("Damien", customerList.findCustomers("01340343")[0].firstName)
    }
}