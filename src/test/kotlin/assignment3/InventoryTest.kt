package assignment3

import assignment3.Domain.Inventory
import assignment3.Seed.DummyDatabase
import assignment3.models.Customer
import assignment3.models.SalesInvoice
import assignment3.models.TaxInvoice
import assignment3.models.Vehicle
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class InventoryTest {

    @BeforeEach
    fun init() {
        DummyDatabase.seedData()
    }

    /**
     * Test that we can get all vehicles stored within the database, regardless of whether they have been sold or not.
     */
    @Test
    fun testGetAllVehicles() {

        val inventory = Inventory()
        assertEquals(11, inventory.getVehicles().size)

    }

    /**
     * Test that we can add and then retrieve the same vehicle.
     */
    @Test
    fun testAddVehicle() {
        val inventory = Inventory()
        val testVehicle = Vehicle(vinNumber = "1234Test", model = "Mustang", manufacturer = "Ford", baseCost = 35000.00)
        inventory.addVehicle(testVehicle)

        assertEquals(12, inventory.getVehicles().size)
        assertTrue(inventory.getVehicles().contains(testVehicle))
        assertEquals(null, inventory.getVehicle(vinNumber = "1324"))
        assertEquals(testVehicle, inventory.getVehicle(vinNumber = "1234Test"))
    }

    /**
     * Test that we can get a vehicle from the inventory, update it, and have that update reflected in the inventory.
     */
    @Test
    fun testUpdateVehicle() {
        val inventory = Inventory()
        val testVehicle = Vehicle(vinNumber = "1234Test", model = "Mustang", manufacturer = "Ford", baseCost = 35000.00)
        inventory.addVehicle(testVehicle)

        val updateVehicle = inventory.getVehicle(vinNumber = "1234Test")

        assertEquals(testVehicle, updateVehicle)

        assertEquals(35000.00, testVehicle.baseCost)
        assertEquals(35000.00, inventory.getVehicle("1234Test")?.baseCost)

        updateVehicle?.baseCost = 24000.14

        assertEquals(24000.14, testVehicle.baseCost)
        assertEquals(24000.14, updateVehicle?.baseCost)
        assertEquals(24000.14, inventory.getVehicle("1234Test")?.baseCost)
    }

    /**
     * Within the database the Mazda 3 and the CX5 should be marked as sold. All other vehicles should be for
     * sale. This method tests that we are able to get the vehicles that are for sale.
     */
    @Test
    fun testVehiclesForSale() {
        val inventory = Inventory()
        val vehiclesForSale = inventory.getVehiclesForSale()

        assertEquals(9, vehiclesForSale.size)

        // The following VIN numbers are sold -
        // "ALDKFJWE3234CSE43" & "995132DS21D6WE2D2"
        val mazda3 = inventory.getVehicle("ALDKFJWE3234CSE43")
        val mazdaCX5 = inventory.getVehicle("995132DS21D6WE2D2")

        // Sale list.
        assertFalse(inventory.getVehiclesForSale().contains(mazda3))
        assertFalse(inventory.getVehiclesForSale().contains(mazdaCX5))

        // All vehicles list.
        assertTrue(inventory.getVehicles().contains(mazda3))
        assertTrue(inventory.getVehicles().contains(mazdaCX5))
    }

    /**
     * Tests that we can add a sales invoice for a vehicle through the inventory.
     */
    @Test
    fun testAddSalesInvoice() {
        val inventory = Inventory()

        val firstVehicleForSale = inventory.getVehiclesForSale().first()
        val customer = Customer(customerId = 3, firstName = "Bruce", lastName = "Banner", address = "1 Test Street, Test State, Test", phoneNumber = "1324")

        val salesInvoice = SalesInvoice(customer = customer, purchaseVehicle = firstVehicleForSale, negotiatedPrice = 3000.0, soldBy = DummyDatabase.salesPeople.first())

        inventory.addInvoice(salesInvoice)

        assertTrue(inventory.getVehicles().contains(firstVehicleForSale))
        assertFalse((inventory.getVehiclesForSale().contains(firstVehicleForSale)))
        assertTrue(DummyDatabase.invoices.contains(salesInvoice))
    }

    /**
     * Test that we can add a tax invoice through the inventory.
     */
    @Test
    fun testAddTaxInvoice() {
        val inventory = Inventory()
        val salesInvoice = DummyDatabase.invoices.first { invoice -> invoice is SalesInvoice }
        val taxInvoice = TaxInvoice(customer = salesInvoice.customer, salesInvoice = salesInvoice as SalesInvoice)

        inventory.addInvoice(taxInvoice)
        assertTrue(DummyDatabase.invoices.contains(taxInvoice))
    }

}